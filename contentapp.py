#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        nombre= request.split('=')[-1]
        print("nombre: ", nombre)
        method = request.split(' ', 2)[0]
        print("method:", method)
        resource = request.split(' ', 2)[1]
        print("reource:", resource)
        return resource,nombre,method


    def process(self, resources):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        resourceName, nombre, method= resources
        formulario = "<p>" + "<form action='' method='POST'><p>" \
                           + "Reescribela con el nombre : <input name ='nombre' type = 'text' />" \
                           + "<input type='submit' value='Submit' />" \
                           + "</body></html>"
        if method == "GET":
            if resourceName in self.content.keys():
                httpCode = "200 OK"
                htmlBody = "<html><body>"\
                           + "<p>La ruta"+ resourceName +" tiene el nombre: " + self.content[resourceName] + "<p>"\
                           +formulario
            else:
                httpCode = "404 Not Found"
                htmlBody = "<p>La ruta"+ resourceName + " no existe<p>" \
                           + formulario
        elif method == "POST":
            self.content[resourceName] = nombre
            print(self.content)
            if resourceName in self.content.keys():
                httpCode = "200 OK"
                htmlBody = "<html><body>"\
                           + "<p>La ruta"+ resourceName +" tiene el nombre: " + self.content[resourceName] + "<p>"\
                           +formulario
            else:
                httpCode = "404 Not Found"
                htmlBody = "<p>La ruta"+ resourceName + " no existe<p>" \
                           + formulario
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
